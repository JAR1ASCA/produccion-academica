const moment = require('moment');
// const User = require('../models/Users');
const helpers = {};

helpers.timeago = timestamp => {

    return moment(timestamp).startOf('second').fromNow();
}

helpers.equalToAdmin = (rol) => {

    if (rol == 'admin') {
        return true;
    } else {
        return false;
    }
}

helpers.ifEqualsTo = function(UserRol, rol) {
    if (UserRol == rol) {
        return true;
    } else {
        return false;
    }
}


helpers.ifEquals = function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
};


module.exports = helpers;