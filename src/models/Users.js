const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
    usuario: { type: String, required: true },
    nombre: { type: String, required: true },
    apellidos: { type: String, required: true },
    rol: { type: String, default: 'colaborador' },
    edad: { type: Number, default: '' },
    direccion: { type: String, default: '' },
    celular: { type: Number, default: '' },
    curp: { type: String, default: '' },
    rfc: { type: String, default: '' },
    cvu: { type: String, default: '' },
    lugarNacimiento: { type: String, default: '' },

    tituloLicenciatura: { type: String, default: '' },
    fechaLicenciatura: { type: String, default: '' },

    tituloMaestria: { type: String, default: '' },
    fechaMaestria: { type: String, default: '' },

    tituloDoctorado: { type: String, default: '' },
    fechaDoctorado: { type: String, default: '' },

    correo: { type: String, required: true },
    contraseña: { type: String, required: true },
    fechaRegistro: { type: Date, default: Date.now },

    estado: { type: Date, default: Date.now },

    usuarioActual: { type: String, default: '' },
    tokenResetPwd: { type: String, default: '' }
});

UserSchema.methods.encryptPassword = async(contraseña) => {
    const salt = await bcrypt.genSalt(10);
    const hash = bcrypt.hash(contraseña, salt);
    return hash;
};

UserSchema.methods.matchPassword = async function(contraseña) {
    return await bcrypt.compare(contraseña, this.contraseña);
}

module.exports = mongoose.model('user', UserSchema);