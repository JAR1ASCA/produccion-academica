const mongoose = require("mongoose");
const { Schema } = mongoose;

const ArticleSchema = new Schema({
    idUser: { type: String, required: true },
    username: { type: String, required: true },
    nombre: { type: String, required: true },
    autor: { type: String, required: true },
    url: { type: String, required: true },
    tipo: { type: String, required: true },
    filename: { type: String },
    fechaRegistro: { type: Date, default: Date.now }
}, {
    timestamps: true
});

module.exports = mongoose.model('articulo', ArticleSchema);