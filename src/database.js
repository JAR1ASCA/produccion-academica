// const { MongoClient } = require('mongodb');
// const uri = "mongodb+srv://user1:<password>@cluster0.sc6np.mongodb.net/produccionAcademica?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
// perform actions on the collection object
//   client.close();
// console.log('db conectada');
// });

//
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(db => console.log('db conectada'))
    .catch(err => console.error(err));