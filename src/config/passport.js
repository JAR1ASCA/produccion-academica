const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/Users');

passport.use(new LocalStrategy({
    usernameField: 'correo',
    passwordField: 'contraseña'
}, async(correo, contraseña, done) => {

    const user = await User.findOne({ correo: correo });

    if (!user) {
        return done(null, false, { message: 'Usuario no encontrado.' });
    } else {
        const match = await user.matchPassword(contraseña);
        if (match) {

            await User.findByIdAndUpdate(user._id, {
                estado: new Date
            });

            console.log("ROL DEL USUARIO = " + user.rol);


            return done(null, user);
        } else {
            return done(null, false, { message: 'Contraseña incorrecta' });
        }
    }
}));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});