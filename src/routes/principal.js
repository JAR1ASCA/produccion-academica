const router = require('express').Router();

const { isAuthenticated } = require('../helpers/auth');

router.get('/principal', isAuthenticated, (req, res) => {
    res.render('principal/principal');
});

module.exports = router;