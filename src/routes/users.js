const router = require('express').Router();

const User = require('../models/Users');

const Article = require('../models/Article');

const passport = require('passport');

const { isAuthenticated } = require('../helpers/auth');

const convTime = require('../helpers/helpers');

var jwt = require('jsonwebtoken');

const nodemailer = require('nodemailer');

router.get('/users/signin', (req, res) => {
    res.render('users/signin');
});



router.post('/users/signin', passport.authenticate('local', {

    successRedirect: '/home',
    failureRedirect: '/users/signin',
    failureFlash: true
}));

router.get('/user-admin', (req, res) => {
    res.render('users/admin');
});


router.get('/users/signup', (req, res) => {
    res.render('users/signup');
});

router.post('/users/signup', async(req, res) => {
    const { usuario, nombre, apellidos, correo, contraseña, confirmar_contraseña } = req.body;
    const errors = [];

    // console.log(req.body);
    if (usuario.length <= 0) {
        errors.push({ text: 'Por favor, ingrese un nombre de usuario' });
    }

    if (nombre.length <= 0) {
        errors.push({ text: 'Por favor, ingrese su nombre' });
    }
    if (apellidos.length <= 0) {
        errors.push({ text: 'Por favor, ingrese sus apellidos' });
    }
    if (contraseña != confirmar_contraseña) {
        errors.push({ text: 'Las contraseñas no coinciden' });
    }
    if (contraseña.length < 6) {
        errors.push({ text: 'La contraseña debe tener al menos 6 caracteres' });
    }

    if (errors.length > 0) {
        res.render('users/signup', { errors, usuario, nombre, apellidos, correo, contraseña, confirmar_contraseña });
    } else {
        const Username = await User.findOne({ usuario: usuario }).lean();
        const UserEmail = await User.findOne({ correo: correo }).lean();

        if (Username) {
            req.flash('error_msg', 'Nombre de usuario no disponible');
            res.redirect('/users/signup');

        } else if (UserEmail) {
            req.flash('error_msg', 'El correo ya se encuentra registrado');
            res.redirect('/users/signup');

        } else {
            const newUser = new User({ usuario, nombre, apellidos, correo, contraseña });
            newUser.contraseña = await newUser.encryptPassword(contraseña);
            await newUser.save();
            req.flash('success_msg', 'Usuario registrado exitosamente');
            res.redirect('/users/signin');
        }
    }
});



router.get('/home', isAuthenticated, async(req, res) => {
    const user = await User.findOne({ _id: req.user.id });

    if (user.rol == 'colaborador') {
        res.redirect('/users/profile');
    } else {

        // await User.findByIdAndUpdate(req.user._id, {
        //     usuarioActual: 'usuarioActual'
        // });

        const arti = await Article.find().sort({ fechaRegistro: -1 }).limit(1);

        await User.updateMany({}, { usuarioActual: req.user.id }, { multi: true });
        console.log("USUARIO LOGUEADO = " + req.user.id);



        var usuarios = await User.find().lean().sort({ apellidos: 1 });
        const articulos = await Article.find({ idUser: req.user.id }).lean().count();

        if (user.rol == 'superAdmin') {
            res.render('users/adminpage', { usuarios, articulos });
        } else {
            res.render('users/adminpage2', { usuarios, articulos }); //admin, admin-colaborador
        }
        // console.log(req.user.id);
        // console.log(usuarios);

    }
});
router.get('/users/profile/:_id', isAuthenticated, async(req, res) => {
    const articulos = await Article.find({ idUser: req.params._id }).lean().count();

    const user = await User.findById(req.user.id);

    await User.findById(req.params._id).
    then(datos => {
        const info = {
            _id: datos._id,
            usuario: datos.usuario,
            nombre: datos.nombre,
            apellidos: datos.apellidos,
            edad: datos.edad,
            direccion: datos.direccion,
            celular: datos.celular,
            curp: datos.curp,
            rfc: datos.rfc,
            cvu: datos.cvu,
            lugarNacimiento: datos.lugarNacimiento,
            correo: datos.correo,

            tituloLicenciatura: datos.tituloLicenciatura,
            fechaLicenciatura: datos.fechaLicenciatura,
            tituloMaestria: datos.tituloMaestria,
            fechaMaestria: datos.fechaMaestria,
            tituloDoctorado: datos.tituloDoctorado,
            fechaDoctorado: datos.fechaDoctorado,

            rol: datos.rol
        }
        res.render('users/profile2', { profile: info, articulos, user });
        // res.render('articles/edit-article', { article: info });
    });
});

router.put('/users/profile/:_id', isAuthenticated, async(req, res) => {
    const { newRol } = req.body;
    await User.findByIdAndUpdate(req.params._id, {
        rol: newRol
    });
    req.flash('success_msg', 'Cuenta actualizada exitosamente');
    res.redirect(`/users/profile/${req.params._id}`);
});








router.get('/users/profile', isAuthenticated, async(req, res) => {

    const usuarios = await User.find().lean().sort({ apellidos: 1 });
    const articulos = await Article.find({ idUser: req.user.id }).lean().count();

    await User.findById(req.user.id)
        .then(datos => {
            const info = {
                _id: datos._id,
                usuario: datos.usuario,
                nombre: datos.nombre,
                apellidos: datos.apellidos,
                edad: datos.edad,
                direccion: datos.direccion,
                celular: datos.celular,
                curp: datos.curp,
                rfc: datos.rfc,
                cvu: datos.cvu,
                lugarNacimiento: datos.lugarNacimiento,
                correo: datos.correo,

                tituloLicenciatura: datos.tituloLicenciatura,
                fechaLicenciatura: datos.fechaLicenciatura,
                tituloMaestria: datos.tituloMaestria,
                fechaMaestria: datos.fechaMaestria,
                tituloDoctorado: datos.tituloDoctorado,
                fechaDoctorado: datos.fechaDoctorado,

                estado: datos.estado1
            }

            res.render('users/profile', { profile: info, usuarios, articulos });
        });
    // console.log(req.userAdmin);
    // console.log(articulos);

});










router.get('/users/profile/edit/:id', isAuthenticated, async(req, res) => {
    await User.findById(req.params.id)
        .then(datos => {
            const info = {
                _id: datos._id,
                usuario: datos.usuario,
                nombre: datos.nombre,
                apellidos: datos.apellidos,
                edad: datos.edad,
                direccion: datos.direccion,
                celular: datos.celular,
                curp: datos.curp,
                rfc: datos.rfc,
                cvu: datos.cvu,
                lugarNacimiento: datos.lugarNacimiento,
                correo: datos.correo
            }
            res.render('users/edit-profile', { profile: info });
        })
});

router.put('/users/profile/edit-profile/:id', isAuthenticated, async(req, res) => {
    const {
        nombre,
        apellidos,
        edad,
        direccion,
        celular,
        curp,
        rfc,
        cvu,
        lugarNacimiento,
        correo
    } = req.body;

    await User.findByIdAndUpdate(req.params.id, {
        nombre,
        apellidos,
        edad,
        direccion,
        celular,
        curp,
        rfc,
        cvu,
        lugarNacimiento,
        correo
    });
    req.flash('success_msg', 'Perfil editado exitosamente');
    res.redirect('/users/profile');
});



router.get('/users/education/edit/:id', isAuthenticated, async(req, res) => {
    await User.findById(req.params.id)
        .then(datos => {
            const info = {
                _id: datos._id,
                tituloLicenciatura: datos.tituloLicenciatura,
                fechaLicenciatura: datos.fechaLicenciatura,
                tituloMaestria: datos.tituloMaestria,
                fechaMaestria: datos.fechaMaestria,
                tituloDoctorado: datos.tituloDoctorado,
                fechaDoctorado: datos.fechaDoctorado

            }
            res.render('users/edit-education', { education: info });
        })
});


router.put('/users/education/edit/:id', isAuthenticated, async(req, res) => {
    const {
        tituloLicenciatura,
        fechaLicenciatura,
        tituloMaestria,
        fechaMaestria,
        tituloDoctorado,
        fechaDoctorado
    } = req.body;
    await User.findByIdAndUpdate(req.params.id, {
        tituloLicenciatura,
        fechaLicenciatura,
        tituloMaestria,
        fechaMaestria,
        tituloDoctorado,
        fechaDoctorado
    });
    req.flash('success_msg', 'Formación académica editada exitosamente');
    res.redirect('/users/profile');
});




router.delete('/users/delete/:_id', isAuthenticated, async(req, res) => {

    await User.findByIdAndDelete(req.params._id);

    console.log("Usuario: " + req.params._id + " eliminado");
    req.flash('success_msg', 'Usuario eliminado exitosamente');
    res.redirect("/home");

});



// router.post('/users/register'); //creo que no sirve para nada


router.get('/reset-password', async(req, res) => {
    res.render('users/reset-pwd');

});

router.post('/reset-password', async(req, res, next) => {
    const correo = req.body.correo;

    const user = await User.findOne({ correo: correo });

    if (!user) {
        req.flash('error_msg', 'El correo no se encuentra registrado en el sistema');
        res.redirect('/reset-password');
    } else {

        var token = jwt.sign({ correo }, process.env.PRIVATE_KEY, { expiresIn: '1h' });

        const host = String(req.hostname);

        const password = String(process.env.PASSWORD);

        let transporter = nodemailer.createTransport({
            service: "gmail",
            port: 465,
            secure: true,
            auth: {
                user: `${process.env.USER}`,
                pass: password
            }
        });


        let message = ({
            from: process.env.USER, // sender address
            to: correo, // list of receivers
            subject: "Restablecer su contraseña", // Subject line
            html: '<html> <head></head><body>' +
                '<div div class = "card" style = "width:400px" > ' +
                '<div class="card-body">' +
                '<h4 class="card-title">Hola ' + user.nombre + ' ' + user.apellidos + ',</h4>' +
                '<p class="card-text">Recibimos una solicitud para restablecer la contraseña de su Cuenta de Usuario de <b>Producción Académica</b>.</p>' +
                '<a href="http://' + host + '/reset-password/' + token + '" >Restablecer Contraseña</a>' +
                '<p class="card-text">Si usted no solicitó este cambio, entonces puede ignorar este mensaje y su contraseña no se restablecerá.</p>' +
                '' +
                '</div>' +
                '</div></body></html>'

        });

        await transporter.sendMail(message, async(error, info) => {
            if (error) {
                console.log('Error occurred');
                console.log(error.message);
                return process.exit(1);
            }

            user.tokenResetPwd = token;
            await user.save();

            console.log('Message sent successfully!');
            console.log(nodemailer.getTestMessageUrl(info));

            req.flash('success_msg', 'Instrucciones enviadas correctamente a su correo');
            res.redirect('/reset-password');
        });


    }
});


router.get('/reset-password/:id', async(req, res) => {

    const token = req.params.id;
    const user = await User.findOne({ tokenResetPwd: token });

    if (!user) {
        req.flash('error_msg', 'Error: El link ya ha sido utilizado o ha expirado!');
        res.redirect('/users/signin');
    } else {
        var info = {
            token: token
        }
        res.render('users/reset-pwd2', { tokenResetPwd: info });
    }
});

router.post('/reset-password2', async(req, res) => {
    const { contraseña, confirmar_contraseña, token } = req.body;

    console.log("TOKEN  = " + token);
    if (contraseña != confirmar_contraseña) {
        req.flash('error_msg', 'Las contraseñas no coinciden');
        res.redirect(`/reset-password/${token}`);

    } else {
        const user = await User.findOne({ tokenResetPwd: token });
        user.contraseña = await user.encryptPassword(contraseña);
        user.tokenResetPwd = '';
        user.save();

        req.flash('success_msg', 'Contraseña restablecida exitosamente');
        res.redirect('/users/signin');
    }
});


router.get('/users/logout', isAuthenticated, (req, res) => {
    req.logOut();
    res.redirect('/');
});





module.exports = router;