const router = require('express').Router();
const User = require('../models/Users');
const Article = require('../models/Article');
const passport = require('passport');
var pdf = require("pdf-creator-node");
const fs = require('fs-extra');


const { isAuthenticated } = require('../helpers/auth');


router.get('/reports', isAuthenticated, async(req, res) => {

    console.log('++++++++++++++++++++++++++++++++++');

    let direction = req.body.direction;
    console.log("direccion" + direction);
    /*
    Month-year-day -> fecha actual
    Month1-year1 -> mes y año actual
    Month2-year2 -> mes anterior y su respectivo año
    */
    let month = 0;
    let month1 = 0;
    if (req.body.month) {
        month1 = Number(req.body.month);
    } else {
        month1 = 15;
    }
    console.log('month1 lectura= ' + req.body.month);
    console.log('month1 nuevo?= ' + month1);
    let month2 = 0;
    let year = 0;
    let year1 = 0;
    year1 = Number(req.body.year);
    console.log('year1 lectura= ' + req.body.year);
    let year2 = 0;
    let flagNow = 0;

    if (month1 > 11) {
        let date = new Date();
        month = date.getMonth();
        year = date.getFullYear();
        flagNow = 1;
        if (month == 11) {
            month1 = 0;
            year1 = year + 1;
            year2 = year;
        } else {
            month1 = month + 1;
            year1 = year;
            year2 = year1;
        }
    } else {

        if (direction == 'back') {
            if (month1 == 0) {
                month2 = 11;
                year2 = year1 - 1;
            } else {
                month2 = month1 - 1;
                year2 = year1;
            }
        } else {
            if (month1 == 11) {
                month2 = 0;
                month1 = 1;
                year2 = year1 + 1;
                year1 = year1 + 2;
            } else {
                month2 = month1 + 1;
                month1 = month1 + 2;
                year2 = year1;
            }
        }
    }

    console.log('month1 despues = ' + month1);
    console.log('month2 despues = ' + month2);
    console.log('year1 despues = ' + year1);
    console.log('year2 despues = ' + year2);


    let articulos = await Article.find({
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();

    console.log('month1 despuesSSSSSSSSSSS = ' + month1);
    console.log('month2 despuesSSSSSSSSSSS = ' + month2);

    console.log("Articulos en mes [" + month2 + "] =" + articulos);

    const articulosT = await Article.find().lean().count();
    console.log("Articulos totales = " + articulosT);

    const usuarios = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();
    console.log("Usuarios en mes [" + month2 + "] =" + usuarios);


    const usuariosT = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }]
    }).lean().count();
    console.log("Usuarios totales =" + usuarios);


    const articles = await Article.find({
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ nombre: 1 });

    let promArt = articulos / usuarios;

    let promArtT = articulosT / usuariosT;

    if (!promArt || usuarios == 0) {
        promArt = 0;
    }
    if (!promArtT || usuariosT == 0) {
        promArtT = 0;
    }

    const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ];

    let monthx = months[month2];

    let fechaR = monthx + '-' + year2;
    console.log("fechaR =" + fechaR);

    var fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;


    console.log(fechaR);


    let info = {
        year: year2,
        year1: year1,
        year2: year2,
        month: monthx,
        month1: month1,
        month2: month2,
        day: 1,
        fecha: fechaR
    }

    let info2 = {
        url: fullUrl
    }


    const usersColl = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ apellidos: 1 });



    const usersAdmin = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ apellidos: 1 });

    const usuariosAdmin = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();

    const usuariosAdminT = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }]


    }).lean().count();

    console.log("USUARIOS ADMINISTRADORES = " + usuariosAdminT);
    console.log("USUARIOS ADMINISTRADORES REGISTRADOS POR MES = " + usuariosAdmin);


    res.render('reports/reporte', { usersColl, usersAdmin, usuarios, usuariosAdmin, usuariosT, usuariosAdminT, articulos, articulosT, promArt, promArtT, articles, date: info, page: info2 });
});













router.post('/reports', isAuthenticated, async(req, res) => {

    console.log('++++++++++++++++++++++++++++++++++');

    let direction = req.body.direction;
    console.log("direccion" + direction);
    /*
    Month-year-day -> fecha actual
    Month1-year1 -> mes y año actual
    Month2-year2 -> mes anterior y su respectivo año
    */
    let month = 0;
    let month1 = 0;
    if (req.body.month) {
        month1 = Number(req.body.month);
    } else {
        month1 = 15;
    }
    console.log('month1 lectura= ' + req.body.month);
    console.log('month1 nuevo?= ' + month1);
    let month2 = 0;
    let year = 0;
    let year1 = 0;
    year1 = Number(req.body.year);
    console.log('year1 lectura= ' + req.body.year);
    let year2 = 0;
    let flagNow = 0;

    if (month1 > 11) {
        let date = new Date();
        month = date.getMonth();
        year = date.getFullYear();
        flagNow = 1;
        if (month == 11) {
            month1 = 0;
            year1 = year + 1;
            year2 = year;
        } else {
            month1 = month + 1;
            year1 = year;
            year2 = year1;
        }
    } else {

        if (direction == 'back') {
            if (month1 == 0) {
                month2 = 11;
                year2 = year1 - 1;
            } else {
                month2 = month1 - 1;
                year2 = year1;
            }
        } else {
            if (month1 == 11) {
                month2 = 0;
                month1 = 1;
                year2 = year1 + 1;
                year1 = year1 + 2;
            } else {
                month2 = month1 + 1;
                month1 = month1 + 2;
                year2 = year1;
            }
        }
    }

    console.log('month1 despues = ' + month1);
    console.log('month2 despues = ' + month2);
    console.log('year1 despues = ' + year1);
    console.log('year2 despues = ' + year2);


    let articulos = await Article.find({
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();

    console.log('month1 despuesSSSSSSSSSSS = ' + month1);
    console.log('month2 despuesSSSSSSSSSSS = ' + month2);

    console.log("Articulos en mes [" + month2 + "] =" + articulos);

    const articulosT = await Article.find().lean().count();
    console.log("Articulos totales = " + articulosT);

    const usuarios = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();
    console.log("Usuarios en mes [" + month2 + "] =" + usuarios);


    const usuariosT = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }]
    }).lean().count();
    console.log("Usuarios totales =" + usuarios);


    const articles = await Article.find({
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ nombre: 1 });

    let promArt = articulos / usuarios;

    let promArtT = articulosT / usuariosT;

    if (!promArt || usuarios == 0) {
        promArt = 0;
    }
    if (!promArtT || usuariosT == 0) {
        promArtT = 0;
    }

    const months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ];

    let monthx = months[month2];

    let fechaR = monthx + '-' + year2;
    console.log("fechaR =" + fechaR);

    var fullUrl = req.protocol + '://' + req.get('Host') + req.originalUrl;


    console.log(fechaR);


    let info = {
        year: year2,
        year1: year1,
        year2: year2,
        month: monthx,
        month1: month1,
        month2: month2,
        day: 1,
        fecha: fechaR
    }

    let info2 = {
        url: fullUrl
    }


    const usersColl = await User.find({
        $or: [{ "rol": "colaborador" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ apellidos: 1 });



    const usersAdmin = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().sort({ apellidos: 1 });

    const usuariosAdmin = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }],
        fechaRegistro: {
            $gte: new Date(year2, month2, 1),
            $lt: new Date(year1, month1, 1)
        }
    }).lean().count();

    const usuariosAdminT = await User.find({
        $or: [{ "rol": "admin" }, { "rol": "superAdmin" }, { "rol": "admin-colaborador" }]


    }).lean().count();

    console.log("USUARIOS ADMINISTRADORES = " + usuariosAdminT);
    console.log("USUARIOS ADMINISTRADORES REGISTRADOS POR MES = " + usuariosAdmin);


    res.render('reports/reporte', { usersColl, usersAdmin, usuarios, usuariosAdmin, usuariosT, usuariosAdminT, articulos, articulosT, promArt, promArtT, articles, date: info, page: info2 });
});



//ESTOY REALIZANDO CAMBIOS EN LAS CONSULTAS DE LOS USUARIOS >>> usar OR para las consultas














router.post('/reports/download/:id', isAuthenticated, async(req, res) => {


    let date = req.body.month2 + "-" + req.body.year;
    console.log("fecha del documento = " + date);

    const thisDate = new Date();
    const thisMonth = thisDate.getMonth();
    const thisYear = thisDate.getFullYear();
    const thisDate2 = thisMonth + "-" + thisYear;
    console.log("esta fecha = " + thisDate2);


    if (fs.existsSync(`./src/public/files/pdf/${req.body.month}-${req.body.year}.pdf`)) { // && date == thisDate2
        console.log("");
        console.log("Son iguales DENTRO");
        console.log("");
        await fs.unlink(`./src/public/files/pdf/${req.body.month}-${req.body.year}.pdf`);
    }

    if (!fs.existsSync(`./src/public/files/pdf/${req.body.month}-${req.body.year}.pdf`)) {


        console.log("");
        console.log("genero PDF Nuevo");
        console.log("");



        let html = fs.readFileSync("./src/views/reports/template.html", "utf8");


        var projectRoot = process.cwd();
        // projectRoot = projectRoot.replace(/\\/g, '/');
        var imgBase = String(projectRoot);

        console.log("DIRECCIÓN DE LA IMAGEN 1 = " + imgBase);



        var linkImage = String(req.hostname);

        console.log("DIRECCIÓN DE LA IMAGEN 2 = " + linkImage);

        let options = {
            format: "A4",
            orientation: "portrait",
            border: "10mm",
            type: "pdf"
        };

        console.log("month = ", req.body.month);

        var fecha = [{
            month: req.body.month,
            year: req.body.year

        }];



        var infogrl = [{
            usuarios: req.body.usuarios,
            usuariosAdmin: req.body.usuariosAdmin,
            usuariosT: req.body.usuariosT,
            usuariosAdminT: req.body.usuariosAdminT,
            articulos: req.body.articulos,
            articulosT: req.body.articulosT,
            promArt: req.body.promArt,
            promArtT: req.body.promArtT,
            linkImage: linkImage
        }];

        let year1 = req.body.year1;
        let year2 = req.body.year2;
        let month1 = req.body.month1;
        let month2 = req.body.month2;

        const articles = await Article.find({
            fechaRegistro: {
                $gte: new Date(year2, month2, 1),
                $lt: new Date(year1, month1, 1)
            }
        }).lean().sort({ nombre: 1 });

        const users = await User.find({
            fechaRegistro: {
                $gte: new Date(year2, month2, 1),
                $lt: new Date(year1, month1, 1)
            }
        }).lean().sort({ apellidos: 1 });

        // const usersAdmin = await User.find({
        //     fechaRegistro: {
        //         $gte: new Date(year2, month2, 1),
        //         $lt: new Date(year1, month1, 1)
        //     }
        // }).lean().sort({ apellidos: 1 });

        console.log("");
        console.log("");
        console.log(articles);


        var document = {
            html: html,
            data: {
                fecha: fecha,
                infogrl: infogrl,
                articles,
                users
            },
            path: `./src/public/files/pdf/${req.body.fecha}.pdf`,
            type: "",
        };

        pdf.create(document, options)
            .then((res) => {
                console.log(res);
            })
            .catch((error) => {
                console.error(error);
            });


    }

    res.redirect(`/reports/download/${req.body.fecha}`);
});

router.get('/reports/download/:id', isAuthenticated, async(req, res) => {

    while (!fs.existsSync(`./src/public/files/pdf/${req.params.id}.pdf`));

    let report = {
        fecha: req.params.id
    };

    console.log("reporte!! fecha!= " + report.fecha);

    res.render('reports/viewReport', { report });

});


module.exports = router;