const router = require('express').Router();
const Article = require('../models/Article');
const User = require('../models/Users');
const passport = require('passport');
const path = require('path');
const fs = require('fs-extra');

// const uploadFile = require('../middleware/multer');

const { isAuthenticated } = require('../helpers/auth');


router.get('/articles/:id', isAuthenticated, async(req, res) => {

    const usuario = await User.findById(req.user.id);


    const articles = await Article.find({ idUser: req.params.id }).lean().sort({ nombre: 1 });


    console.log("USUARIO LOGUEADO = " + req.user.id);
    console.log("USUARIO REQUERIDO = " + req.params.id);

    var idUser = {
        id: req.params.id
    };

    res.render('articles/all-articles', { articles, thisUser: idUser, usuario });
});

// router.get('/articles/:_id', isAuthenticated, async(req, res) => {
//     const articles = await Article.find({ idUser: req.params._id }).lean().sort({ nombre: 1 });
//     res.render('articles/all-articles2', { articles });
// });

router.get('/articles_new-article', isAuthenticated, (req, res) => {

    var user = {
        id: req.user.id
    }
    res.render('articles/registerArticle', { thisUser: user });
});

router.post('/articles_new-article', isAuthenticated, async(req, res) => {
    const { nombre, autor, url, tipo } = req.body;

    const articleUrl = Date.now();
    const articlePath = req.file.path;
    const ext = path.extname(req.file.originalname).toLowerCase();
    const targetPath = path.resolve(`./src/public/files/${articleUrl}${ext}`);

    if (ext == '.pdf') {
        await fs.rename(articlePath, targetPath);

        const newArticle = new Article({ nombre, autor, url, tipo });
        newArticle.idUser = req.user.id;
        newArticle.username = req.user.usuario;
        newArticle.filename = articleUrl + ext;

        const articleSaved = await newArticle.save();
        console.log(newArticle);

        req.flash('success_msg', 'Articulo registrado exitosamente');
        res.redirect(`/articles/${req.user.id}`);

    } else {
        await fs.unlink(articlePath);
        req.flash('error_msg', 'El archivo seleccionado no es de tipo PDF');
        res.redirect('/articles_new-article');
    }


});

router.get('/articles/view/:_id', isAuthenticated, async(req, res) => {

    console.log("ID DEL USUARIO SOLICITADO" + req.body.idUser);

    console.log("ID DEL ARTICULO SOLICITADO" + req.params._id);

    const PDF = await Article.findOne({ _id: req.params._id });
    console.log(PDF);

    // var pathActual = req.path;

    if (PDF && fs.existsSync(`./src/public/files/${PDF.filename}`)) {
        res.render('articles/viewArticle', { PDF });
    } else {
        req.flash('error_msg', 'Artículo no encontrado');
        res.redirect(`/articles/${req.body.idUser}`);
    }
});


router.post('/articles/view/:_id', isAuthenticated, async(req, res) => {

    console.log("ID DEL USUARIO SOLICITADO" + req.body.idUser);

    console.log("ID DEL ARTICULO SOLICITADO" + req.params._id);

    const PDF = await Article.findOne({ _id: req.params._id });
    console.log(PDF);

    // var pathActual = req.path;

    if (PDF && fs.existsSync(`./src/public/files/${PDF.filename}`)) {
        res.render('articles/viewArticle', { PDF });
    } else {
        req.flash('error_msg', 'Artículo no encontrado');
        res.redirect(`/articles/${req.body.idUser}`);
    }
});




router.get('/articles/edit/:_id', isAuthenticated, async(req, res) => {
    await Article.findById(req.params._id).
    then(datos => {
        const info = {
            _id: datos._id,
            nombre: datos.nombre,
            autor: datos.autor,
            tipo: datos.tipo,
            url: datos.url,
            idUser: datos.idUser

        }
        res.render('articles/edit-article', { article: info });
    });
});

router.put('/articles/edit/:_id', isAuthenticated, async(req, res) => {
    const { nombre, autor, tipo, url } = req.body;

    if (req.file) {
        console.log('si');

        const articleUrl = Date.now();
        const articlePath = req.file.path;
        const ext = path.extname(req.file.originalname).toLowerCase();
        const targetPath = path.resolve(`./src/public/files/${articleUrl}${ext}`);

        if (ext == '.pdf') {
            const article = await Article.findOne({ _id: req.params._id });
            if (article.filename) {
                await fs.unlink(`./src/public/files/${article.filename}`);
            }

            console.log(article);
            console.log(article.filename);

            await fs.rename(articlePath, targetPath);

            await Article.findByIdAndUpdate(req.params._id, { nombre: nombre, autor: autor, tipo: tipo, url: url, filename: articleUrl + ext });

            req.flash('success_msg', 'Artículo actualizado exitosamente');

            res.redirect(`/articles/${req.body.idUser}`);

        } else {
            await fs.unlink(articlePath);
            req.flash('error_msg', 'El archivo seleccionado no es de tipo PDF');
            res.redirect(`/articles/edit/${req.params._id}`);
        }
    } else {
        await Article.findByIdAndUpdate(req.params._id, { nombre: nombre, autor: autor, tipo: tipo, url: url });

        console.log(req.file);

        req.flash('success_msg', 'Artículo actualizado exitosamente');
        res.redirect(`/articles/${req.body.idUser}`);

    }
});

router.delete('/articles/delete/:_id', isAuthenticated, async(req, res) => {
    const article = await Article.findOne({ _id: req.params._id });

    if (article.filename && fs.existsSync(`./src/public/files/${article.filename}`)) {
        await fs.unlink(`./src/public/files/${article.filename}`);
    }
    await Article.findByIdAndDelete(req.params._id);

    req.flash('success_msg', 'Artículo eliminado exitosamente');
    res.redirect(`/articles/${article.idUser}`);

});



module.exports = router;